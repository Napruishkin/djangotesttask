from django.urls import path
from .views import AttributeListCreateView, AttributeRetrieveUpdateDestroyView, ProductAttributeValueListCreateView, ProductAttributeValueRetrieveUpdateDestroyView

urlpatterns = [
    path('attributes/', AttributeListCreateView.as_view(), name='attribute_list_create'),
    path('attributes/<int:pk>/', AttributeRetrieveUpdateDestroyView.as_view(), name='attribute_retrieve_update_destroy'),
    path('product-attributes/', ProductAttributeValueListCreateView.as_view(), name='product_attribute_value_list_create'),
    path('product-attributes/<int:pk>/', ProductAttributeValueRetrieveUpdateDestroyView.as_view(), name='product_attribute_value_retrieve_update_destroy'),
]