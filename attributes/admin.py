from django.contrib import admin
from .models import Attribute, ProductAttributeValue

admin.site.register(Attribute)
admin.site.register(ProductAttributeValue)
