from django.db import models
from categories.models import Category


class Product(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    main_image = models.ImageField(upload_to='products/')
    additional_images = models.ManyToManyField('ProductImage', blank=True)
    categories = models.ManyToManyField(Category, blank=True)

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class ProductImage(models.Model):
    image = models.ImageField(upload_to='products/')

    def __str__(self):
        return self.image.name
