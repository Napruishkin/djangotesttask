from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from .forms import SignUpForm, ProfileForm, SignInForm
from .models import Profile


@csrf_exempt
def signup(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        if User.objects.filter(email=email).exists():
            return JsonResponse({'error': 'Email already exists'})

        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return JsonResponse({'message': 'Sign up successful'})
        else:
            errors = form.errors.items()
            error_messages = [error[1] for error in errors]
            return JsonResponse({'error': error_messages[0]})
    else:
        form = SignUpForm()
    return render(request, 'signup.html', {'form': form})


@csrf_exempt
def signin(request):
    if request.method == 'POST':
        form = SignInForm(data=request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            user = authenticate(request, email=email, password=password)
            if user is not None:
                login(request, user)
                return JsonResponse({'message': 'Sign in successful'})
            else:
                return JsonResponse({'error': 'Invalid email or password'})
        else:
            return JsonResponse({'error': form.errors})
    else:
        form = SignInForm()
    return render(request, 'signin.html', {'form': form})


@login_required
def profile(request):
    if request.method == 'POST':
        try:
            profile = request.user.profile
        except Profile.DoesNotExist:
            # create a new profile for the user
            profile = Profile.objects.create(user=request.user)

        form = ProfileForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()
            return JsonResponse({'message': 'Profile updated'})
        else:
            errors = form.errors.items()
            error_messages = [error[1] for error in errors]
            return JsonResponse({'error': error_messages[0]})
    else:
        form = ProfileForm(instance=request.user.profile)
    return render(request, 'profile.html', {'form': form})
