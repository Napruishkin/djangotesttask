# E-Commerce API using Django and SQL Database
### This project contains a basic e-commerce API built with Django and an SQL database.

## Setup

1. Install the required packages:
```shell
pip install -r requirements.txt
```
2. Create a new SQL database and update the DATABASES setting in settings.py with the appropriate credentials.
3. Apply the database migrations:
```shell
python manage.py migrate
```
4. Start the development server:
```shell
python manage.py runserver
```

## Entities
The following entities have been implemented:

## Auth
- Sign in endpoint (email/password)
- Sign up endpoint (email/password/password confirmation)

## Product
- CRUD operations
- Fields: name (required), description (required), price (required), main image (required), additional images (optional), categories (optional, more than one)

## Categories
- CRUD operations
- Infinite tree of categories and subcategories
- Fields: name (required), description (required), other needed fields

## Optional Point - Attributes System
- A separate "attributes" entity
- Ability to create attributes and assign them a custom value when creating a product
Example:

- 2 products: phone, laptop
- 1 attribute: width
- - For the phone the value will be "80mm"
- - For a laptop, the value will be "200mm"

## API Documentation
The API documentation can be found at /docs/ (e.g. http://localhost:8000/docs/).

## Docker
To run the application in a Docker container, follow these steps:

1. Build the Docker image:
```shell
docker-compose build
```
2. Start the Docker container:
```shell
docker-compose up
```
Note: The Dockerfile and docker-compose.yml files are included in the project for convenience, but you may need to modify them depending on your specific requirements.